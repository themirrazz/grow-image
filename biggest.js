/*
Twitter: https://pbs.twimg.com/media/abcde-fghij?format=png&name=large
Trip Advisor: https://media-cdn.tripadvisor.com/media/photo-o/12/34/56/78/photo.jpg
Trip Advisor Medium Image: https://media-cdn.tripadvisor.com/media/photo-m/1234/12/34/56/78/phoyo.jpg
*/

(function(href){
    var url=new URL(href);
    if(url.hostname==="pbs.twimg.com"&&url.pathname.startsWith("/media/")) {
        url.searchParams.set("name","orig");
    } else if(url.hostname==="dynamic-media-cdn.tripadvisor.com"||url.hostname==="media-cdn.tripadvisor.com") {
        if(url.pathname.split("/")[2]==="photo-o"&&url.hostname!="dynamic-media-cdn.tripadvisor.com") { return null }
        return "https://media-cdn.tripadvisor.com/media/photo-o/"+((
            url.pathname.split("/").slice(url.pathname.split("/").length-5)
        ).join("/"));
    } else if(url.hostname==="i.pinimg.com") {
        if(url.pathname.split("/")[1]!="originals") {
            url.pathname="/originals/"+(url.pathname.split("/").slice(2).join("/"))
        } else {
            return null
        }
    } else {
        return null
    }
    return url.href
})(location.href)
